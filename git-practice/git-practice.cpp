// git-practice.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

static const char *INPUT_FILE = "input.txt";
static const char *OUTPUT_FILE = "output.txt";

template <typename T>
class functor
{
	// not implemented
};

template <typename T>
class predicate
{
	// not implemented
};

template <typename T>
class printer
{
	// not implemented
};

template <typename T>
class input_reader 
{
	// not implemented
};

template <typename T>
class output_writer
{
	// not implemented
};

int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<int> source_v;
	std::vector<decltype(source_v.begin())> target_v;

	input_reader<decltype(source_v.begin())> read;
	// source_v = read(INPUT_FILE);

	predicate<decltype(source_v.begin())> pred;
	std::copy_if(source_v.begin(), source_v.end(), target_v.begin(), pred);

	functor<decltype(source_v.begin())> func;
	std::for_each(target_v.begin(), target_v.end(), func);

	printer<decltype(source_v.begin())> printr;
	std::for_each(target_v.begin(), target_v.end(), printr);

	output_writer<decltype(source_v.begin())> write;
	// write(OUTPUT_FILE, target_v);

	return 0;
}

